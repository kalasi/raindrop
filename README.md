# Raindrop

Raindrop is a network service for generating unique IDs at _incredibly_ high
scale, similar to Twitter Snowflake.


## Performance

- minimum 100k ids per second per process
- response rate 0.2ms (plus network latency)


## Coordination

Machines do not need to be coordinated to generate these IDs, and as such there
is no implementation to do such. Each raindrop instance simply needs a unique
instance number.


## Sortable

IDs are roughly sortable. For the biggest guarentee on sortability, ensure that
all machines are properly configured in regards to the Unix timestamp.


## Compact

All IDs are under 64 bits by a large enough margin.


## Requirements

A stable version of Rust is recommended. Probably all stable versions work.


## Usage

Where the machine ID (alternatively, instance ID) is an 8-bit integer and the
port number is the port number to listen to.
`cargo run --release --bin raindrop <machine_id> <port_number>`


## License

ISC
