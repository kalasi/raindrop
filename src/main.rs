// ISC License (ISC)
//
// Copyright (c) 2016, Austin Hellyer <hello@austinhellyer.me>
//
// Permission to use, copy, modify, and/or distribute this software for any
// purpose with or without fee is hereby granted, provided that the above
// copyright notice and this permission notice appear in all copies.
//
// THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
// WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
// SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER
// RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF
// CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
// CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

extern crate time;

use std::io::Write;
use std::net::{TcpListener, TcpStream};
use std::{env, thread};

static COUNTER_MAX: u64 = 1024;
static mut COUNTER: u64 = 0;

fn main() {
    // The machine ID is the server number (or instance number) of raindrop.
    //
    // This _should_ be unique across all raindrop services.
    let machine_id: u64 = env::args()
        .nth(1)
        .expect("No Machine ID given")
        .parse::<u64>()
        .expect("Machine ID must be an 8-bit integer");

    // The port number to listen to for this instance.
    //
    // This _must_ be unique for all raindrop instances on this server.
    let port = env::args()
        .nth(2)
        .expect("No port to listen to")
        .parse::<u64>()
        .expect("Port must be an integer");

    let listener = TcpListener::bind(&format!("127.0.0.1:{}", port)[..])
        .expect("Error configuring listener");

    for stream in listener.incoming() {
        match stream {
            Err(e) => println!("Incoming connection failed: {}", e),
            Ok(stream) => {
                thread::spawn(move || {
                    handle_client(stream, machine_id)
                });
            },
        }
    }
}

/// Handles each client connection by calculating a unique* UID and then sending
/// the UID back as a response.
///
/// * = mostly always
fn handle_client(mut stream: TcpStream, machine_id: u64) {
    // Incrementing the counter is technically unsafe. While this has the
    // possibility of producing duplicate UIDs, someone would need to be
    // generating K * V UIDs per second on a single worker, where K is the
    // number of units per second (1000) and V is the maximum value of the
    // counter.
    //
    // In other words, a lot.
    //
    // As a side-effect of this implementation, this has the possibility of
    // overflowing by slight amounts. In exchange for this, we gain a
    // performance increase.
    let counter: u64 = unsafe {
        if COUNTER == COUNTER_MAX {
            COUNTER = 0;
        } else {
            COUNTER += 1;
        }

        COUNTER
    };

    // Calculate the number of milliseconds since the epoch.
    let ms: u64 = {
        let time = time::get_time();

        (time.sec as u64 * 1000) + (time.nsec as u64 / 1_000_000)
    };

    let millis: u64 = ms * 1024;
    let id: u64 = machine_id * 256;
    let uid: u64 = millis + id + counter;

    let _result = stream.write_all(&uid.to_string().into_bytes());
}
